﻿using UnityEngine;

/*
 * github/gitlab/telegram - @kotC9
 */
namespace Controllers
{
    public class GunController : MonoBehaviour
    {
        public int force;
        public int offsetX;
        public int offsetY;
        public int offsetZ;

        private float _timer = 0;
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && Time.time - _timer > 1.0f)
            {
                ThrowBall();
                _timer = Time.time;
            }
        }

        private void ThrowBall()
        {
            var box = GameObject.CreatePrimitive(PrimitiveType.Cube);
            box.AddComponent<Rigidbody>();
            box.transform.position = transform.position;
            box.transform.rotation = transform.rotation;
            box.gameObject.name = "Box";

            var boxPos = box.transform.position;
            var direction = new Vector3(boxPos.x+offsetX,boxPos.y+offsetY,boxPos.z+offsetZ);
            box.GetComponent<Rigidbody>().AddForce(direction*force);
        }
    }
}
