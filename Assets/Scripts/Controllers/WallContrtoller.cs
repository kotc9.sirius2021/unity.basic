﻿using System;
using UnityEngine;

/*
 * github/gitlab/telegram - @kotC9
 */
namespace Controllers
{
    public class WallContrtoller : MonoBehaviour
    {
        public void OnTriggerEnter(Collider other)
        {
            Debug.Log($"Wall triggered with {other.gameObject.name}");
            //столкновение только с боксами
            if (other.gameObject.name == "Box")
            {
                GetComponent<Renderer>().material.color = 
                    new Color(LockedRandom.NextFloat(), LockedRandom.NextFloat(), LockedRandom.NextFloat());
                Destroy(other.gameObject);
            }
        }
    }
}