﻿using System;

/*
 * github/gitlab/telegram - @kotC9
 */
public static class LockedRandom
{
    private static readonly Random Random = new Random();


    public static float NextFloat()
    {
        lock (Random)
        {
            return (float) Random.NextDouble();
        }
    }
}